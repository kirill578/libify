
var opened = [false, false, false, false, false, false];

function cool(theIndex) {
  if(!opened[theIndex]) {
    $( "#card_details_" + theIndex).slideDown({
      duration: 1000,
      easing: "easeInOutQuart"
    });
    $('#github_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "100",
      height: "100",
      top: "0",
      left: "0"
    }, 1200); 
    $('#linkedin_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "100",
      height: "100",
      top: "0",
      left: "0"
    }, 1200);
    $('#sof_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "100",
      height: "100",
      top: "0",
      left: "0"
    }, 1200);
    $('#meetup_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "100",
      height: "100",
      top: "0",
      left: "0"
    }, 1200);


  } else {
    $( "#card_details_" + theIndex).slideUp({
      duration: 1000,
      easing: "easeInOutQuart"
    });
    $('#github_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "1",
      height: "1",
      top: "50",
      left: "50"
    }, 1200);
    $('#meetup_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "1",
      height: "1",
      top: "50",
      left: "50"
    }, 1200);
    $('#sof_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "1",
      height: "1",
      top: "50",
      left: "50"
    }, 1200);
    $('#linkedin_logo_' + theIndex).animate({
      easing: 'easeOutBounce',
      width: "1",
      height: "1",
      top: "50",
      left: "50"
    }, 1200);
  }
  opened[theIndex] = !opened[theIndex];
}
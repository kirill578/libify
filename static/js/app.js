angular.module('libifyApp', ['routerRoutes','ngMaterial'])

.controller('mainController', function($rootScope) {
	var vm = this;
})

.controller('cardController', function ($scope, $rootScope, $timeout) {
  var self = this;
  self.cardIndex = $scope.$index;
  self.data = $rootScope.queryResults[self.cardIndex];
})

.controller('ResultController', function($scope, $rootScope, $http, $location) {
  var self = this;
  self.results = [];

  self.init = function () {
    if($rootScope.librariesQuery == undefined) { return $location.path('/'); }
    $http.get("/search?q=" + $rootScope.librariesQuery.join(',')).
    success(function(data) {
      self.results = data.data;
      $rootScope.queryResults = self.results;
    });
  };
})

.controller('SearchController', function($location, $rootScope, $http) {
  var self = this;
  self.readonly = false;
  self.tags = [];

  self.querySearch = function (querySearch) {
    return $http.get("/auto-complete?q=" + querySearch).then(function (result) {
      return result.data.data;
    });
  }

  self.search = function() {
    $rootScope.librariesQuery = self.tags;
    $location.path('/results');
  };
})

.directive('cardControl', function(){
  return {
    restrict: 'E',
    templateUrl: '../templates/card-control.html'
  }
})
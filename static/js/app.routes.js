angular.module('routerRoutes', ['ngRoute'])

// configure our routes
.config(function($routeProvider, $locationProvider) {
    $routeProvider
        // route for the home page
        .when('/', {
            templateUrl : '../templates/search.html',
            controller  : 'SearchController'
        })

	  .when('/results', {
            templateUrl : '../templates/results.html',
        });



});
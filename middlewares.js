var express = require('express');
module.exports = function (app) {
	
	app.use(express.static(__dirname + '/static'));
	

	app.use(function (req, res, next) {
	  res.rest = function (status, data_or_error) {
	     var word = (status?"data":"error");
	     retObj = {};
	     retObj.status = status;
	     retObj[word] = data_or_error;
		 res.json(retObj);
	  }
	  next();
	});


	app.use(function(req, res, next) {
		req.checkParams = function (params, callback) {
			for(var i in params)
			{
				if(req.query[params[i]] == undefined)
				{
					res.rest(false, "undefined parameter: `" + params[i] + "`");
					return false;
				}
					
			}
			callback();
			return true;
		}
		next();
	});
};
var http = require('http');
var express = require('express');
var app = express();
var path = require('path');
var Model = require('./models/model')();

app.set('port', (process.env.PORT || 3000));
app.set('view engine', 'vash');
require('./middlewares')(app);

app.get('/libraries', function (req, res) {
	Model.Libraries.find({}, function (err, docs) {
		res.rest(true, docs);
	});
});

app.get('/tags', function (req, res) {
	Model.Tags.find({}, {name: 1, _id: 0},function (err, docs) {
		res.rest(true, docs);
	});
});

//example: /auto-complete?q=no
app.get('/auto-complete', function(req, res) {
	req.checkParams(['q'], function () {
		Model.Tags.find({}, function (err, docs) {
			var tags = []
			docs.forEach(function(doc) {if(doc.name.toUpperCase().indexOf(req.query.q.toUpperCase()) != -1) tags.push(doc.name);});
			res.rest(true, tags.slice(0,5));
		});
	});
});

//example: /search?q=nodejs,iphone
app.get('/search', function(req, res){
	req.checkParams(['q'], function () {
		Model.Libraries.find({"tags":{"$all": req.query.q.split(',')}}, function (err, docs) {
			res.rest(true, docs);
		});
	});
});

app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname + '/static/index.html'));
});

var server = http.createServer(app);
server.listen(app.get('port'), function () {
	console.log('Node app is running on port', app.get('port'));
});
var Model = require('./../models/model')();
var create_mocks = require('./create_mocks.js');
var api_worker = require('./api_worker');
var tag_worker = require('./tag_indexer_worker.js');


console.log('Creating Mocks...')
create_mocks(Model, function () {
	console.log('Updating API Data...')
	api_worker(Model, function () {
		console.log('re-indexing Tags...');
		tag_worker(Model, function () {
			console.log('Done!');
			process.exit();
		});
	});
});

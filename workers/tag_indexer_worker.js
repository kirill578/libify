module.exports = function (Model, callback)
{
	function indexTags()
	{
		var tags = {};
		Model.Libraries.find({}, function (err, docs) {
			docs.forEach(function (doc) {
				doc.tags.forEach(function (tag){
					if(tags[tag] == undefined) tags[tag] = [];
					tags[tag].push(doc._id);
				});
			});
			var dbObj = [];
			for(tag in tags)
			{
				dbObj.push({
					name: tag,
					libraries: tags[tag]
				});
			}
			Model.Tags.create(dbObj, function (err,res) {
				console.log('Tags Updated');
				callback();
			});
		});
	}

	Model.Tags.find({} , function(err, docs) {
		var count = docs.length;
		if(count==0) indexTags();
		docs.forEach(function (doc) {
			doc.remove(function () {
				count--;
				if(count==0)
				{
					console.log('Tags Cleared');
					indexTags();
				}
			});
		});
	});





}



var github = require('./apis/github');
var stackoverflow = require('./apis/stackoverflow');


module.exports = function(Model, callback)
{
	function finish() {
		console.log('Api Updated');
		callback();
	}

	Model.Libraries.find({}, function (err, docs) {
		console.log('Processing ' + docs.length + ' Records...');
		var count = docs.length * 2;
		if(count == 0) finish();
		docs.forEach(function (doc) {

			console.log('Github: Requesting ' + doc.githubStats.githubId);
			github.getRepositoryInfo(doc.githubStats.githubId, function (data){
				count--;
				doc.githubStats.forks = data.forks;
				doc.githubStats.watchers = data.wachers;
				doc.githubStats.openIssues = data.open_issues;
				doc.save();
				console.log('Github: ' + doc.githubStats.githubId + ' Updated');
				if(count == 0) finish();
			});

			console.log('Stackoverflow: Requesting ' + doc.stackStats.stackId);
			stackoverflow.getQuestionsByTag(doc.stackStats.stackId, function (data) {
				count--;
				doc.stackStats.questions = data.questions;
				doc.stackStats.answers = 0;
				doc.save();
				console.log('Stackoverflow: ' + doc.stackStats.stackId + ' Updated');
				if(count == 0) finish();
			});
		});
	});

}


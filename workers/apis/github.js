var request = require('request');

module.exports = {

	// repoId means :owner/:repository_name
	getRepositoryInfo: function(repoId, success, failure){
		var options = {
			url: 'https://api.github.com/repos/' + repoId,
			headers: {
				'User-Agent': 'request'
			}
		};

		request(options, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var info = JSON.parse(body);

				var repoInfo = {};
				repoInfo.description = info.description;
				repoInfo.name = info.name;
				repoInfo.avatar_url = info.owner.avatar_url;
				repoInfo.forks = info.forks;
				repoInfo.watchers = info.watchers;
				repoInfo.open_issues = info.open_issues;


				success(repoInfo);
			}
			else{
				failure(error);
			}
		});


	}

}
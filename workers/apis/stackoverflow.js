var request = require('request');
// request.debug = true;
module.exports = {

	getQuestionsByTag: function(tag, success, failure){
		var options = {
			gzip: true,
			url: 'https://api.stackexchange.com/2.2/tags/'+tag+'/info?order=desc&sort=popular&site=stackoverflow',
			headers: {
				'User-Agent': 'request',
				'Accept-Encoding': 'gzip'
			}
		};

		request(options, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var info = JSON.parse(body);

				var tagInfo = {};
				tagInfo.name = info.items[0].name;
				tagInfo.questions = info.items[0].count;

				success(tagInfo);
			}
			else{
				failure(error);
			}
		});


	}

}
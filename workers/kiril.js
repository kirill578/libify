function Lib() {
    this.title= ''
    this.description= ''
    this.ranking= 0,
    this.tags= []
    this.githubStats= {
      githubId: '',
      contributors: 0,
      forks: 0,
      watchers: 0,
      openIssues: 0
    }
    this.stackStats=
    {
      stackId: '',
      questions: 0,
      answers: 0
    }
    this.eventsStats=
    {
      eventId: '',
      groups: 0,
      events: 0
    }
    this.linkedInStats=
    {
      tagId: '',
      endorsements: 0,
      jobs: 0
    }
  };


var json = [
  {
    "title":"Picasso",
    "description":"A powerful image downloading and caching library for Android",
    "in_jobs":"15",
    "in_endors":"40",
    "in_groups":0,
    "meetup_events":201,
    "meetup_groups":3,
    "github_contribu":45,
    "github_watch":"3.1k",
    "github_forks":48,
    "github_issues":125,
    "sof_q":"1248",
    "sof_a":"458",
    "tags":"android, network, java, mobile"
  },
  {
    "title":"October",
    "description":"is a Content Management System (CMS) and web platform whose sole purpose is to make your development workflow simple again.",
    "in_jobs":"400",
    "in_endors":"25",
    "in_groups":1,
    "meetup_events":180,
    "meetup_groups":8,
    "github_contribu":58,
    "github_watch":"45k",
    "github_forks":94,
    "github_issues":65,
    "sof_q":"350",
    "sof_a":"13",
    "tags":"php, cms, web"
  },
  {
    "title":"Drupal",
    "description":"Drupal is an open source content management platform supporting a variety of websites ranging from personal weblogs to large community-driven websites",
    "in_jobs":"6241",
    "in_endors":"85k",
    "in_groups":48,
    "meetup_events":2152,
    "meetup_groups":48,
    "github_contribu":987,
    "github_watch":"245k",
    "github_forks":451,
    "github_issues":984,
    "sof_q":"34k",
    "sof_a":"3k",
    "tags":"php, cms, web"
  },
  {
    "title":"OkHttp",
    "description":"An HTTP & SPDY client for Android and Java applications",
    "in_jobs":"12k",
    "in_endors":"3k",
    "in_groups":400,
    "meetup_events":2451,
    "meetup_groups":40,
    "github_contribu":584,
    "github_watch":"8k",
    "github_forks":40,
    "github_issues":454,
    "sof_q":"1k",
    "sof_a":"200",
    "tags":"android, java, network, images, mobile"
  },
  {
    "title":"Universal Image Loader",
    "description":"UIL aims to provide a powerful, flexible and highly customizable instrument for image loading, caching and displaying",
    "in_jobs":"12k",
    "in_endors":"3k",
    "in_groups":584,
    "meetup_events":400,
    "meetup_groups":1,
    "github_contribu":201,
    "github_watch":"20k",
    "github_forks":655,
    "github_issues":458,
    "sof_q":"8k",
    "sof_a":"3k",
    "tags":"android, java, network, images, mobile"
  },
  {
    "title":"otto",
    "description":"An enhanced Guava-based event bus with emphasis on Android support.",
    "in_jobs":"12k",
    "in_endors":"3k",
    "in_groups":420,
    "meetup_events":865,
    "meetup_groups":3,
    "github_contribu":12,
    "github_watch":"12",
    "github_forks":45,
    "github_issues":66,
    "sof_q":"425",
    "sof_a":"85",
    "tags":"android, java, framework, mobile"
  },
  {
    "title":"LeakCanary",
    "description":"A memory leak detection library for Android and Java.",
    "in_jobs":"6k",
    "in_endors":"1k",
    "in_groups":201,
    "meetup_events":784,
    "meetup_groups":4,
    "github_contribu":64,
    "github_watch":"54",
    "github_forks":4,
    "github_issues":5,
    "sof_q":"150",
    "sof_a":"45",
    "tags":"android, java, preformance, mobile"
  },
  {
    "title":"NineOldAndroids",
    "description":"Android library for using the Honeycomb (Android 3.0) animation API on all versions of the platform back to 1.0!",
    "in_jobs":"2k",
    "in_endors":"3k",
    "in_groups":120,
    "meetup_events":642,
    "meetup_groups":4,
    "github_contribu":21,
    "github_watch":"32",
    "github_forks":98,
    "github_issues":1,
    "sof_q":"325",
    "sof_a":"21",
    "tags":"android, java, animation, mobile"
  },
  {
    "title":"Feature API",
    "description":"",
    "in_jobs":"",
    "in_endors":"",
    "in_groups":null,
    "meetup_events":null,
    "meetup_groups":null,
    "github_contribu":null,
    "github_watch":"",
    "github_forks":null,
    "github_issues":null,
    "sof_q":"",
    "sof_a":"",
    "tags":""
  }
];


for(var i in json)
{
    var j = json[i];
    var c = new Lib();
    console.log(c);

}


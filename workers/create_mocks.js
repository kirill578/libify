var mocks = [{
	title: "NodeJs",
	description: "A superb runtime",
	ranking: "9.5", 
	tags: ['nodejs', 'js', 'javascript', 'server-side'],
	githubStats: {
		githubId: 'nodejs/node',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'nodejs',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'node',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "Express",
	description: "A superb framework",
	ranking: "9.5", 
	tags: ['web', 'mvc', 'javascript', 'server-side', 'middle-ware'],
	githubStats: {
		githubId: 'strongloop/express',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'express',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'express',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "AngularJs",
	description: "Development platform for building mobile and desktop web applications",
	ranking: "9.5", 
	tags: ['mobile', 'android','iphone','desktop','client-side'],
	githubStats: {
		githubId: 'angular/angular',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'angularjs',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'angularjs',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "Picasso",
	description: "A powerful image downloading and caching library for Android",
	ranking: "6.2",
	tags: ['mobile', 'android','network', 'client-side', 'java'],
	githubStats: {
		githubId: 'square/picasso',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'picasa',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'Picasso',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "Jquery",
	description: "The best lib for number addition",
	ranking: "9.5", 
	tags: ['javascript', 'client-side','number-addition'],
	githubStats: {
		githubId: 'jquery/jquery',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'jquery',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'jquery',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "OkHttp",
	description: "An HTTP & SPDY client for Android and Java applications",
	ranking: "5",
	tags: ['open-source', 'mobile','java', 'android', 'http', 'network'],
	githubStats: {
		githubId: 'square/okhttp',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'okhttp',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'OkHttp',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "Bootstrap",
	description: "Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.",
	ranking: "5",
	tags: ['open-source', 'mobile','javascript', 'css', 'responsive-ui', 'ui'],
	githubStats: {
		githubId: 'twbs/bootstrap',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'twitter-bootstrap',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'Bootstrap',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "BackboneJS",
	description: "Give your JS App some Backbone with Models, Views, Collections, and Events",
	ranking: "5",
	tags: ['open-source','javascript', 'mvc'],
	githubStats: {
		githubId: 'jashkenas/backbone',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'twitter-bootstrap',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'Bootstrap',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "Django",
	description: "Django makes it easier to build better Web apps more quickly and with less code.",
	ranking: "5",
	tags: ['python','mvc', 'web', 'web-framework', 'server-side'],
	githubStats: {
		githubId: 'django/django',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'django',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'Django',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
},
{
	title: "Boost",
	description: "Django makes it easier to build better Web apps more quickly and with less code.",
	ranking: "5",
	tags: ['c++','smart-pointers', 'cross-platform', 'asio'],
	githubStats: {
		githubId: 'mirror/boost',
		contributors: 0,
		forks: 0,
		watchers: 0,
		openIssues: 0
	},
	stackStats:
	{
		stackId: 'boost',
		questions: 0,
		answers: 0
	},
	eventsStats:
	{
		eventId: 'Boost',
		groups: 0,
		events: 0
	},
	linkedInStats:
	{
		tagId: '',
		endorsements: 0,
		jobs: 0
	}
}];

function postMock(model, callback) {
	console.log('DB cleared, creating mocks...');
	model.Libraries.create(mocks, function (err, res) {
		callback();
	});
}

module.exports = function(model, callback)
{
	model.Libraries.find({}, function (err, docs) {
		console.log('Clearing collection')
		count = docs.length;
		if(count == 0)
		{
			postMock(model, callback);
			return;
		}
		docs.forEach(function (doc) {
			count--;
			doc.remove();
			if(count == 0)
			{
				postMock(model, callback);
			}
		});
	});
}

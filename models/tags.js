module.exports = function(mongoose) {

	var Schema = mongoose.Schema;

	var TagSchema = new Schema({
		name: String,
		libraries: Array	
	});

	return mongoose.model('Tags', TagSchema);
}
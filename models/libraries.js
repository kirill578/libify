module.exports = function(mongoose)
{
  var Schema = mongoose.Schema;

  // create a schema
  var librarySchema = new Schema({
    title: String,
    description: String,
    ranking: Number, 
    tags: Array,
    githubStats: {
      githubId: String,
      contributors: Number,
      forks: Number,
      watchers: Number,
      openIssues: Number
    },
    stackStats:
    {
      stackId: String,
      questions: Number,
      answers: Number
    },
    eventsStats:
    {
      eventId: String,
      groups: Number,
      events: Number
    },
    linkedInStats:
    {
      tagId: String,
      endorsements: Number,
      jobs: Number
    }
  });

  // the schema is useless so far
  // we need to create a model using it
  return mongoose.model('Libraries', librarySchema);
}
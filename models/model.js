var mongoose = require('mongoose');

var uristring = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/Libify';
mongoose.connect(uristring);
var db = mongoose.connection;
console.log("Loading Model...");

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
	console.log('DB is now ready');
});


module.exports = function() {
	var Libraries = require('./libraries')(mongoose);
	var Tags = require('./tags')(mongoose);

	return {
		Libraries: Libraries,
		Tags: Tags,
		mongoose: mongoose
	}
}